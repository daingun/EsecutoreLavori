# Diagrammi UML

## Casi d'uso

```plantuml
@startuml
:Utente: -> (Esegui una lista di lavori\ndata una configurazione)
@enduml
```

## Diagrammi di attività 

Funzione principale
```plantuml
@startuml
start
:Creazione registro;
:Generazione lista di attività;
:Verifica aciclicità configurazione;
:Creazione code;
note left
    coda di esecuzione
    coda dei risultati
end note
fork
    :Esecutore;
    note left : eseguito in un\nthread separato
fork again
    :Direttore;
endfork
stop
@enduml
```

Generatore attività
```plantuml
@startuml
start
if (configurazione) then (file)
    :Recupero file;
else (testo)
endif
:Creazione buffer di lettura del file;
:Generazione espressione regolare;
group Generazione attività
    while (configurazione disponibile?) is (sì)
        :Leggi blocco XML;
        :Ottieni di dati del lavoro;
        :Genera singola attività;
    endwhile (no)
end group
:Genera terminazione;
:Restituisci collezione di attività;
stop
@enduml
```

Esecutore
```plantuml
start
:Creazione servizio esecuzione;
:Creazione registro;
fork
group Invia risultati
    repeat :Prendi risultato;
        :Invia risultato;
    repeat while (terminazione) is (no) not (sì)
end group
fork again
group Esecuzione
    repeat :Prendi lavoro;
    backward:Sottometti lavoro;
    repeat while (terminazione) is (no) not (sì)
end group
end fork
stop
```

Direttore
```plantuml
@startuml
start
:Creazione registro;
while (fine lavori?) is (no)
    group Determina lavori disponibili
    :Seleziona attività non in errore;
    :Seleziona attività senza dipendenze;
    :Rimuovi attività selezionate;
    end group
    :Accoda lavori;
    group Gestisci errori
    :Seleziona attività in errore;
    :Seleziona attività senza dipendenze;
    :Segnala completamento con errore;
    :Rimuovi attività selezionate;
    end group
    :Prendi risultato;
    group Segnala completamento
    :Rimuovi dipendenze;
    end group
endwhile (sì)
stop
@enduml
```

## Diagrammi delle classi

### Lavori

Gerarchia tipi di lavoro

```plantuml
@startuml
class Attività {
ILavoro lavoro
void rimuoviDipendenza(String id, boolean errore)
boolean senzaDipendenze()
boolean inErrore()
}
interface ILavoro {
String id()
Collection<String> incompatibilità()
}
interface Callable<Risultato> {
Risultato call()
}
class Calcolo
class PreparazioneDati
class Invio
class Terminazione
class Risultato {
String id
Tipo tipo
Map<String, String> dati
}
enum Tipo {
SUCCESSO
ERRORE
TERMINAZIONE
}

Attività *-- ILavoro
ILavoro -|> Callable
ILavoro <|.. Calcolo
ILavoro <|.. PreparazioneDati
ILavoro <|.. Invio
ILavoro <|.. Terminazione
Tipo -* Risultato
Calcolo *-- Risultato
PreparazioneDati *-- Risultato
Invio *-- Risultato
Terminazione *-- Risultato
@enduml
```

## Diagrammi di sequenze

Interazione tra Direttore ed Esecutore. Viene indicata la sequenza logica delle attività, senza considerare i thread multipli e il parallelismo nell'esecuzione dei lavori.

```plantuml
@startuml
participant Direttore
queue "Coda di\nesecuzione" as CodaE
queue "Coda dei\nrisultati" as CodaR
participant Esecutore
participant "Servizio\nesecuzione" as Serv
participant "Invio risultati" as Inv
Direttore -> CodaE : aggiungi lavoro
Direttore -> Direttore : gestisci errori
CodaE -> Esecutore : prendi lavoro
Esecutore -> Serv : sottometti lavoro
Serv -> Inv : aspetta risultato
Inv -> CodaR : accoda risultato
Direttore <- CodaR : prendi risultato
Direttore -> Direttore : segnala\ncompletamento
@enduml
```

## File di configurazione

Il file di configurazione è in formato XML con la seguente struttura, p:

```
<?xml version="1.0" encoding="UTF-8"?>
<configurazione>
    <lavoro>
        <id>[id]</id>
        <tipo>[tipo]</tipo>
        <parametri>[parametri]</parametri>
        <dipendenze-deboli>[lista-dipendenze-deboli]</dipendenze-deboli>
        <dipendenze-forti>[lista-dipendenze-forti]</dipendenze-forti>
        <incompatibilità>[lista-incompatibilità]</incompatibilità>
    </lavoro>
    <lavoro>
    ...
    </lavoro>
</configurazione>
```

```
* id = 1*(LETTERA / CIFRA / "_")
* tipo = "Calcolo" / "PreparazioneDati" / "Invio"
* parametri = "" / (parametro *(",", parametro))
* parametro = chiave, ":", valore
* chiave = (LETTERA / "_"), *(LETTERA / CIFRA / "_")
* valore = 1*(LETTERA / CIFRA / "_")
* lista-dipendenze-deboli = "" / (id *(",", id))
* lista-dipendenze-forti = "" / (id *(",", id))
* lista-incompatibilità = "" / (tipo *(",", tipo))
```

```plantuml
@startebnf
!pragma compact
id = {LETTERA | CIFRA | "_"}-;
tipo = "Calcolo" | "PreparazioneDati" | "Invio";
parametri = [parametro, {",", parametro}];
parametro = chiave, ":", valore;
chiave = (LETTERA | "_"), {LETTERA | CIFRA | "_"};
valore = {LETTERA | CIFRA | "_"}-;
lista-dipendenze-deboli = [id, {",", id}];
lista-dipendenze-forti = [id, {",", id}];
lista-incompatibilita = [tipo, {",", tipo}];
@endebnf
```

Possono essere passati dei parametri come chiave:valore.  
La lista delle dipendenze deve essere composta dagli identificativi degli altri lavori.  
La lista delle incompatibilità deve essere composta da tipi di lavoro.


## Grafi di test

Grafo delle dipendenze per testare l'aciclicità.
L'arco tratteggiato crea un ciclo.

```plantuml
@startuml
hide empty description
state A
B --> A
B -up[dashed]-> E
C --> A
D --> B
E --> B
E --> C
F --> C
T --> A
T --> B
T --> C
T --> D
T --> E
T --> F
@enduml
```


## Versione PlantUML
```plantuml
@startuml
version
@enduml
```
