# Informazioni

## Descrizione

Programma per la gestione dell'esecuzione parallela di lavori interconnessi da dipendenze.

## Esecuzione

Per eseguire il programma lanciare il seguente comando:

    java -jar EsecutoreLavori.jar ./configurazione.xml [--livelloRegistro 5]
    java -jar EsecutoreLavori.jar --aiuto

## Utilizzo

Il programma legge la configurazione da un file, in cui vengono determinate le dipendenze.

I lavori vengono realizzati tramite classi che realizzano l'interfaccia `ILavoro`.