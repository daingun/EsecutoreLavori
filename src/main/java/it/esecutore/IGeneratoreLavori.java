package it.esecutore;

import java.io.UncheckedIOException;

/**
 * Interfaccia per le classi che generano i lavori a partire dai dati di configurazione.
 */
public interface IGeneratoreLavori {

    /**
     * Genera un lavoro a partire dalle informazioni testuali.
     *
     * @param datiLavoro dati del lavoro.
     * @return un lavoro.
     * @throws UncheckedIOException se la riga non segue le specifiche o se il tipo di lavoro non esiste.
     */
    ILavoro generaLavoro(DatiLavoro datiLavoro) throws UncheckedIOException;
}
