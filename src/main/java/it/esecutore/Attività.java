package it.esecutore;

import java.util.Collection;
import java.util.stream.Stream;

/**
 * Classe che contiene un lavoro e le sue dipendenze.
 */
public final class Attività implements Comparable<Attività> {
    /**
     * Lavoro collegato all'attività.
     */
    public final ILavoro lavoro;

    /**
     * Collezione delle dipendenze deboli del lavoro.
     */
    private final Collection<String> dipendenzeDeboli;

    /**
     * Collezione delle dipendenze forti del lavoro.
     */
    private final Collection<String> dipendenzeForti;

    /**
     * Indica se l'attività non deve essere eseguita a causa di un errore
     * in una dipendenza forte.
     */
    private boolean inErrore = false;

    /**
     * Crea un'attività.
     *
     * @param lavoro           lavoro da eseguire.
     * @param dipendenzeDeboli collezione delle dipendenze deboli del lavoro.
     * @param dipendenzeForti  collezione delle dipendenze forti del lavoro.
     */
    public Attività(ILavoro lavoro, Collection<String> dipendenzeDeboli, Collection<String> dipendenzeForti) {
        this.lavoro = lavoro;
        this.dipendenzeDeboli = dipendenzeDeboli;
        this.dipendenzeForti = dipendenzeForti;
    }

    /**
     * @return Collezione di tutte le dipendenze dell'attività.
     */
    public Stream<String> dipendenze() {
        return Stream.concat(dipendenzeDeboli.stream(), dipendenzeForti.stream());
    }

    /**
     * Rimuovi una dipendenza del lavoro dato il suo identificativo.
     *
     * @param id     identificativo della dipendenza.
     * @param errore indica se la dipendenza ha terminato con un errore.
     */
    public void rimuoviDipendenza(String id, boolean errore) {
        this.dipendenzeDeboli.remove(id);
        if (this.dipendenzeForti.remove(id) && errore) {
            this.inErrore = true;
        }
    }

    /**
     * Determina se l'attività è senza dipendenze.
     *
     * @return vero se l'attività non ha dipendenze.
     */
    public boolean senzaDipendenze() {
        return this.dipendenzeDeboli.isEmpty() && this.dipendenzeForti.isEmpty();
    }

    /**
     * Determina se l'attività ha un errore.
     *
     * @return vero se l'attività ha un errore.
     */
    public boolean inErrore() {
        return this.inErrore;
    }

    @Override
    public int compareTo(Attività other) {
        return String.CASE_INSENSITIVE_ORDER.compare(this.lavoro.id(), other.lavoro.id());
    }
}
