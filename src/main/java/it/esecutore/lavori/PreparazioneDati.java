package it.esecutore.lavori;

import it.esecutore.ILavoro;
import it.esecutore.Registro;
import it.esecutore.Risultato;

import java.util.Collection;
import java.util.Map;

/**
 * Classe di tipo ILavoro che esegue la preparazione dei dati.
 */
public final class PreparazioneDati implements ILavoro {
    /**
     * Identificativo.
     */
    private final String id;

    /**
     * Parametri.
     */
    private final Map<String, String> parametri;

    /**
     * Collezione delle incompatibilità del lavoro.
     */
    private final Collection<String> incompatibilità;

    /**
     * Crea un lavoro di tipo PreparazioneDati.
     *
     * @param id              identificativo del lavoro.
     * @param parametri       parametri per il lavoro.
     * @param incompatibilità collezione delle incompatibilità del lavoro.
     */
    public PreparazioneDati(String id, Map<String, String> parametri, Collection<String> incompatibilità) {
        this.id = id;
        this.parametri = parametri;
        this.incompatibilità = incompatibilità;
    }

    @Override
    public String id() {
        return this.id;
    }

    @Override
    public Collection<String> incompatibilità() {
        return this.incompatibilità;
    }

    @Override
    public Risultato lavora() throws Exception {
        var registro = Registro.OttieniRegistro();
        registro.info("avviato");
        Thread.sleep(2000);
        if (this.parametri.get("div").equals("002")) {
            registro.severe(String.format("Errore nel lavoro %s", this.id));
            return new Risultato(this.id, Risultato.Tipo.ERRORE);
        } else {
            registro.info("completato");
            return new Risultato(this.id, Risultato.Tipo.SUCCESSO);
        }
    }
}
