package it.esecutore.lavori;

import it.esecutore.ILavoro;
import it.esecutore.Registro;
import it.esecutore.Risultato;

import java.util.Collection;
import java.util.Map;

/**
 * Classe di tipo ILavoro che esegue l'invio dei dati.
 */
public final class Invio implements ILavoro {
    /**
     * Identificativo.
     */
    private final String id;

    /**
     * Parametri.
     */
    private final Map<String, String> parametri;

    /**
     * Collezione delle incompatibilità del lavoro.
     */
    private final Collection<String> incompatibilità;

    /**
     * Crea un lavoro di tipo Invio.
     *
     * @param id              identificativo del lavoro.
     * @param parametri       parametri per il lavoro.
     * @param incompatibilità collezione delle incompatibilità del lavoro.
     */
    public Invio(String id, Map<String, String> parametri, Collection<String> incompatibilità) {
        this.id = id;
        this.parametri = parametri;
        this.incompatibilità = incompatibilità;
    }

    @Override
    public String id() {
        return this.id;
    }

    @Override
    public Collection<String> incompatibilità() {
        return this.incompatibilità;
    }

    @Override
    public Risultato lavora() throws Exception {
        var registro = Registro.OttieniRegistro();
        registro.info("avviato");
        registro.info(String.format("parametri %s", this.parametri));
        Thread.sleep(1500);
        registro.info("completato");
        return new Risultato(this.id, Risultato.Tipo.SUCCESSO);
    }
}
