package it.esecutore.analizzatore;

import it.esecutore.DatiLavoro;
import it.esecutore.IAnalizzatore;
import it.esecutore.Registro;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.BufferedReader;
import java.util.*;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.stream.Collectors;

/**
 * Classe per analizzare la configurazione in formato XML.
 */
public final class AnalizzatoreXML implements IAnalizzatore {

    /**
     * Parola chiave della configurazione che identifica un singolo lavoro.
     */
    public static final String LAVORO = "lavoro";
    /**
     * Parola chiave della configurazione che indica l'identificativo del lavoro.
     */
    public static final String ID = "id";
    /**
     * Parola chiave della configurazione che indica il tipo di lavoro.
     */
    public static final String TIPO = "tipo";
    /**
     * Parola chiave della configurazione che indica i parametri del lavoro.
     */
    public static final String PARAMETRI = "parametri";
    /**
     * Parola chiave della configurazione che indica le dipendenze deboli del lavoro.
     */
    public static final String DIPENDENZE_DEBOLI = "dipendenze-deboli";
    /**
     * Parola chiave della configurazione che indica le dipendenze forti del lavoro.
     */
    public static final String DIPENDENZE_FORTI = "dipendenze-forti";
    /**
     * Parola chiave della configurazione che indica le incompatibilità del lavoro.
     */
    public static final String INCOMPATIBILITÀ = "incompatibilità";

    /**
     * Crea un analizzatore di configurazione in formato XML.
     */
    public AnalizzatoreXML() {
    }

    /**
     * Trasforma una lista di valori testuali separati da virgole in una collezione
     * di valori di testo. Ignora gli spazi bianchi attorno alle parole.
     * <p>
     * Esempio: "a,b, c,d " -> ["a", "b", "c", "d"]
     *
     * @param valori testo contenente un lista di valori separati da virgole.
     * @return lista di valori testuali
     */
    private static String[] listaDiValori(String valori) {
        return valori.strip().split("\\s*,\\s*");
    }

    /**
     * Estrai un dizionario chiave-valore dal testo della configurazione.
     *
     * @param gruppo testo della configurazione.
     * @return Dizionario.
     */
    private static Map<String, String> estraiParametri(String gruppo) {
        var valori = Arrays.stream(listaDiValori(gruppo));
        return valori
                .filter(Predicate.not(String::isBlank))
                .map(s -> {
                    var chiaveValore = s.strip().split("\\s*:\\s*");
                    if (chiaveValore.length == 1) {
                        // Nel caso non ci sia ":" o ci sia un testo bianco, restituisci
                        // solo la chiave senza valore.
                        return new DatiLavoro.ChiaveValore(chiaveValore[0], "");
                    } else {
                        return new DatiLavoro.ChiaveValore(chiaveValore[0], chiaveValore[1]);
                    }
                })
                .collect(Collectors.toMap(DatiLavoro.ChiaveValore::chiave, DatiLavoro.ChiaveValore::valore));
    }


    /**
     * Genera la collezione di attività a partire dal file di configurazione.
     *
     * @param configurazione file di configurazione.
     * @return collezione dei dati dei lavori da eseguire.
     */
    @Override
    public Collection<DatiLavoro> generaDatiLavoro(BufferedReader configurazione) {
        var registro = Registro.OttieniRegistro();
        ArrayList<DatiLavoro> datiLavori = new ArrayList<>();
        DatiLavoro datiLavoro = new DatiLavoro();

        try {
            XMLInputFactory factory = XMLInputFactory.newFactory();
            XMLEventReader streamReader = factory.createXMLEventReader(configurazione);
            while (streamReader.hasNext()) {
                XMLEvent e = streamReader.nextEvent();
                if (e.isStartElement()) {
                    StartElement startElement = e.asStartElement();
                    switch (startElement.getName().getLocalPart()) {
                        case LAVORO:
                            datiLavoro = new DatiLavoro();
                            break;
                        case ID:
                            e = streamReader.nextEvent();
                            datiLavoro.id = e.asCharacters().getData();
                            break;
                        case TIPO:
                            e = streamReader.nextEvent();
                            datiLavoro.tipo = e.asCharacters().getData();
                            break;
                        case PARAMETRI:
                            e = streamReader.nextEvent();
                            if (e.isEndElement()) continue;
                            var s = estraiParametri(e.asCharacters().getData());
                            datiLavoro.parametri.putAll(s);
                            break;
                        case DIPENDENZE_DEBOLI:
                            e = streamReader.nextEvent();
                            if (e.isEndElement()) continue;
                            var separati = listaDiValori(e.asCharacters().getData());
                            datiLavoro.dipendenzeDeboli.addAll(List.of(separati));
                            break;
                        case DIPENDENZE_FORTI:
                            e = streamReader.nextEvent();
                            if (e.isEndElement()) continue;
                            var separati2 = listaDiValori(e.asCharacters().getData());
                            datiLavoro.dipendenzeForti.addAll(List.of(separati2));
                            break;
                        case INCOMPATIBILITÀ:
                            e = streamReader.nextEvent();
                            if (e.isEndElement()) continue;
                            var separati3 = listaDiValori(e.asCharacters().getData());
                            datiLavoro.incompatibilità.addAll(List.of(separati3));
                            break;
                        default:
                            break;
                    }
                }
                if (e.isEndElement()) {
                    EndElement endElement = e.asEndElement();
                    if (endElement.getName().getLocalPart().equals(LAVORO)) {
                        datiLavori.add(datiLavoro);
                    }
                }
            }
            registro.info(datiLavori.stream()
                                    .map(DatiLavoro::toString)
                                    .collect(Collectors.joining(System.lineSeparator())));
            return datiLavori;
        } catch (XMLStreamException ex) {
            registro.log(Level.SEVERE, "Eccezione", ex);
            return new ArrayList<>();
        }
    }
}
