package it.esecutore;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.logging.Level;

/**
 * Interfaccia che determina i metodi richiesti per un lavoro
 */
public interface ILavoro extends Callable<Risultato> {
    /**
     * Restituisci l'identificativo del lavoro.
     *
     * @return l'identificativo del lavoro.
     */
    String id();

    /**
     * Restituisci la lista delle incompatibilità.
     *
     * @return collezione delle incompatibilità.
     */
    Collection<String> incompatibilità();

    /**
     * Esegui il lavoro.
     *
     * @return Risultato del lavoro.
     * @throws Exception nel caso di errori inaspettati.
     */
    Risultato lavora() throws Exception;

    @Override
    default Risultato call() {
        try {
            return lavora();
        } catch (Exception ex) {
            var registro = Registro.OttieniRegistro();
            registro.log(Level.SEVERE, "Eccezione", ex);
            return new Risultato(this.id(), Risultato.Tipo.ERRORE, Map.of("Eccezione", ex.getLocalizedMessage()));
        }
    }
}
