package it.esecutore;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.function.Predicate;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Classe che dirige la sequenza di esecuzione dei lavori e ne gestisce il risultato.
 */
public final class Direttore {
    /**
     * Collezione di attività con i valori da far eseguire.
     */
    private final Collection<Attività> listaAttività;

    /**
     * Collezione di lavori correntemente in esecuzione.
     */
    private final Set<String> lavoriInEsecuzione;

    /**
     * Registro informazioni.
     */
    private final Logger registro;

    /**
     * Coda dei lavori da eseguire.
     */
    private final BlockingQueue<ILavoro> codaDiEsecuzione;

    /**
     * Coda dei risultati dei lavori eseguiti.
     */
    private final BlockingQueue<Risultato> codaDeiRisultati;

    /**
     * Crea una classe che dirige l'esecuzione dei lavori.
     *
     * @param listaAttività    collezione di attività da dirigere.
     * @param codaDiEsecuzione coda dei lavori da eseguire.
     * @param codaDeiRisultati coda dei risultati dei lavori.
     */
    public Direttore(Collection<Attività> listaAttività, BlockingQueue<ILavoro> codaDiEsecuzione, BlockingQueue<Risultato> codaDeiRisultati) {
        this.listaAttività = listaAttività;
        this.lavoriInEsecuzione = new HashSet<>();

        this.codaDiEsecuzione = codaDiEsecuzione;
        this.codaDeiRisultati = codaDeiRisultati;

        this.registro = Registro.OttieniRegistro();
    }

    /**
     * Determina se ci sono ancora dei lavori da eseguire.
     *
     * @return vero se ci sono ancora dei lavori da eseguire.
     */
    private boolean fineLavori() {
        return this.listaAttività.isEmpty();
    }

    /**
     * Restituisce la lista dei lavori che possono essere eseguiti,
     * cioè che non hanno dipendenze da soddisfare.
     *
     * @return lista di lavori.
     */
    private Collection<ILavoro> lavoriDisponibili() {
        List<ILavoro> lista = listaAttività.stream()
                                           .filter(Predicate.not(Attività::inErrore))
                                           .filter(Attività::senzaDipendenze)
                                           .map(a -> a.lavoro)
                                           .collect(Collectors.toList());
        // I lavori mandati in esecuzione devono essere tolti dalla lista dei lavori.
        // Altrimenti chiamate successive di questo metodo restituirebbero più volte
        // gli stessi lavori.
        var idLavori = lista.stream().map(ILavoro::id).collect(Collectors.toSet());
        this.listaAttività.removeIf(a -> idLavori.contains(a.lavoro.id()));
        this.lavoriInEsecuzione.addAll(idLavori);

        return lista;
    }

    /**
     * Gestisci le attività con errori. Se il lavoro di un'attività è in errore,
     * completa l'attività propagando l'errore alle relative dipendenze.
     */
    private void gestisciErrori() {
        Set<String> lavoriInErrore = listaAttività.stream()
                                                  .filter(Attività::inErrore)
                                                  .filter(Attività::senzaDipendenze)
                                                  .map(a -> a.lavoro.id())
                                                  .collect(Collectors.toSet());
        for (String id : lavoriInErrore) {
            this.segnalaCompletamento(new Risultato(id, Risultato.Tipo.ERRORE));
            this.registro.warning(String.format("Il lavoro '%s' è stato ignorato a causa di un errore in una sua dipendenza forte.", id));
        }
        this.listaAttività.removeIf(a -> lavoriInErrore.contains(a.lavoro.id()));
    }

    /**
     * Gestisci il risultato di un lavoro: cancella il lavoro terminato dalle
     * dipendenze degli altri lavori e gestisci gli errori dei lavori.
     *
     * @param risultato risultato del lavoro concluso.
     */
    private void segnalaCompletamento(Risultato risultato) {
        boolean errore = risultato.tipo == Risultato.Tipo.ERRORE;
        for (Attività attività : this.listaAttività) {
            attività.rimuoviDipendenza(risultato.id, errore);
        }
        this.lavoriInEsecuzione.removeIf(l -> l.equals(risultato.id));
    }

    /**
     * Esegui i compiti del Direttore: seleziona i lavori da inviare all'esecutore
     * e gestisci i risultati dei lavori.
     */
    public void dirigi() {
        while (!this.fineLavori()) {
            Collection<ILavoro> ld = this.lavoriDisponibili();
            this.codaDiEsecuzione.addAll(ld);
            this.registro.info("Lavori in esecuzione: " + this.lavoriInEsecuzione.toString());
            this.gestisciErrori();
            try {
                Risultato risultato = this.codaDeiRisultati.take();
                this.segnalaCompletamento(risultato);
            } catch (InterruptedException e) {
                this.registro.severe("La coda di uscita è stata interrotta");
                throw new RuntimeException(e);
            }
        }
    }
}