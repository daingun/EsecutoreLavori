package it.esecutore;

import java.util.Map;

/**
 * Classe che definisce il risultato di un lavoro.
 *
 * @see ILavoro
 */
public final class Risultato {
    /**
     * Identificativo del lavoro che ha generato il risultato.
     */
    public final String id;

    /**
     * Tipo di risultato.
     */
    public final Tipo tipo;

    /**
     * Contenitore per possibili dati addizionali del risultato.
     */
    public final Map<String, String> dati;

    /**
     * Crea un risultato di un lavoro.
     *
     * @param id   identificativo del lavoro.
     * @param tipo tipo di risultato.
     * @param dati informazioni aggiuntive.
     */
    Risultato(String id, Tipo tipo, Map<String, String> dati) {
        this.id = id;
        this.tipo = tipo;
        this.dati = dati;
    }

    /**
     * Crea un risultato di un lavoro.
     *
     * @param id   identificativo del lavoro.
     * @param tipo tipo di risultato.
     */
    public Risultato(String id, Tipo tipo) {
        this.id = id;
        this.tipo = tipo;
        this.dati = Map.of();
    }

    /**
     * Tipo di risultato del lavoro.
     */
    public enum Tipo {
        /**
         * Il lavoro ha avuto successo.
         */
        SUCCESSO,
        /**
         * Il lavoro è terminato con un errore.
         */
        ERRORE,
        /**
         * Il lavoro segnala di essere l'ultimo e il programma può terminare.
         */
        TERMINAZIONE
    }
}
