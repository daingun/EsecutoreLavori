package it.esecutore;

import java.util.Collection;
import java.util.List;

/**
 * Classe di tipo ILavoro che simboleggia la terminazione del programma.
 *
 * @param id identificativo del lavoro.
 */
public record Terminazione(String id) implements ILavoro {

    @Override
    public Collection<String> incompatibilità() {
        return List.of();
    }

    @Override
    public Risultato lavora() {
        var registro = Registro.OttieniRegistro();
        registro.info("terminazione lavori");
        return new Risultato(this.id, Risultato.Tipo.TERMINAZIONE);
    }
}
