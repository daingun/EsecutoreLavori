package it.esecutore;

import java.io.BufferedReader;
import java.util.Collection;

/**
 * Interfaccia per le classi che analizzano il file di configurazione.
 */
public interface IAnalizzatore {
    /**
     * Genera la collezione di attività a partire dal file di configurazione.
     *
     * @param configurazione file di configurazione.
     * @return collezione dei dati dei lavori da eseguire.
     */
    Collection<DatiLavoro> generaDatiLavoro(BufferedReader configurazione);
}
