package it.esecutore;

import java.util.concurrent.*;
import java.util.logging.Logger;

/**
 * Classe che gestisce l'esecuzione in parallelo dei lavori.
 */
public final class Esecutore {
    /**
     * Coda dei lavori da eseguire.
     */
    private final BlockingQueue<ILavoro> codaDiEsecuzione;

    /**
     * Coda dei risultati dei lavori eseguiti.
     */
    private final BlockingQueue<Risultato> codaDeiRisultati;

    /**
     * Servizio di esecuzione dei lavori.
     */
    private final CompletionService<Risultato> servizioDiCompletamentoCondiviso;

    /**
     * Esecutore dei lavori.
     */
    private final ExecutorService gruppoDiThread;

    /**
     * Registro informazioni.
     */
    private final Logger registro;

    /**
     * Crea una classe di esecuzione dei lavori.
     *
     * @param codaDiEsecuzione coda dei lavori da eseguire.
     * @param codaDeiRisultati coda dei risultati dei lavori.
     * @param nThread          numero di thread.
     */
    public Esecutore(BlockingQueue<ILavoro> codaDiEsecuzione, BlockingQueue<Risultato> codaDeiRisultati, int nThread) {
        this.codaDiEsecuzione = codaDiEsecuzione;
        this.codaDeiRisultati = codaDeiRisultati;
        this.gruppoDiThread = Executors.newFixedThreadPool(nThread);
        this.servizioDiCompletamentoCondiviso = new ExecutorCompletionService<>(gruppoDiThread);

        this.registro = Registro.OttieniRegistro();
    }

    /**
     * Esegue l'esecutore dei lavori: quando un lavoro è disponibile nella coda di
     * entrata, viene inviato al servizio di esecuzione.
     * L'esecuzione termina quando riceve un lavoro "terminale".
     */
    public void esegui() {
        Thread t = new Thread(this::inviaRisultati);
        t.start();
        while (true) {
            try {
                ILavoro lavoro = this.codaDiEsecuzione.take();
                this.servizioDiCompletamentoCondiviso.submit(lavoro);
                if (lavoro instanceof Terminazione) {
                    // Spegni l'esecutore altrimenti il programma non termina.
                    this.gruppoDiThread.shutdown();
                    t.join();
                    registro.info("completato");
                    return;
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * Raccoglie i risultati quando pronti e li invia alla coda di uscita.
     */
    private void inviaRisultati() {
        while (true) {
            try {
                // Aspetta che ci sia un risultato.
                Risultato risultato = this.servizioDiCompletamentoCondiviso.take().get();
                // Invia il risultato sulla coda di uscita
                this.codaDeiRisultati.put(risultato);
                if (risultato.tipo == Risultato.Tipo.TERMINAZIONE) {
                    // Non ci sono più lavori da eseguire.
                    registro.info("completato");
                    return;
                }
            } catch (InterruptedException | ExecutionException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
