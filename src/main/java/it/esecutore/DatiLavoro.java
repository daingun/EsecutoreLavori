package it.esecutore;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Classe dati che trasforma una stringa di testo in un lavoro
 */
public final class DatiLavoro {
    /**
     * Parametri per il lavoro.
     */
    public final Map<String, String> parametri = new HashMap<>();
    /**
     * Collezione di lavori dipendenze deboli.
     */
    public final Collection<String> dipendenzeDeboli = new ArrayList<>();
    /**
     * Collezione di lavori dipendenze forti.
     */
    public final Collection<String> dipendenzeForti = new ArrayList<>();
    /**
     * Collezione d'incompatibilità.
     */
    public final Collection<String> incompatibilità = new ArrayList<>();
    /**
     * Identificativo del lavoro.
     */
    public String id = "";
    /**
     * Tipo di lavoro.
     */
    public String tipo = "";

    @Override
    public String toString() {
        return "DatiLavoro{" +
                "id='" + id + '\'' +
                ", tipo='" + tipo + '\'' +
                ", parametri=" + parametri +
                ", dipendenzeDeboli=" + dipendenzeDeboli +
                ", dipendenzeForti=" + dipendenzeForti +
                ", incompatibilità=" + incompatibilità +
                '}';
    }

    /**
     * Classe contenente una chiave e un valore.
     *
     * @param chiave Chiave.
     * @param valore Valore.
     */
    public record ChiaveValore(String chiave, String valore) {
    }
}
