package it.esecutore.generatore;

import it.esecutore.DatiLavoro;
import it.esecutore.IGeneratoreLavori;
import it.esecutore.ILavoro;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Classe per generare un lavoro.
 */
public final class GeneratoreLavori implements IGeneratoreLavori {

    /**
     * Elenco di classi che possono essere utilizzate per generare un lavoro.
     */
    private final List<Class<? extends ILavoro>> classi;

    /**
     * Cra un nuovo generatore di lavori.
     */
    public GeneratoreLavori() {
        classi = new ArrayList<>();
    }

    /**
     * Genera un lavoro a partire dalle informazioni testuali.
     *
     * @param datiLavoro dati del lavoro.
     * @return un lavoro.
     * @throws UncheckedIOException se la riga non segue le specifiche o se il tipo di lavoro non esiste.
     */
    @Override
    public ILavoro generaLavoro(DatiLavoro datiLavoro) throws UncheckedIOException {
        try {
            return classi.stream()
                         .filter(c -> c.getSimpleName().equals(datiLavoro.tipo))
                         .findFirst()
                         .orElseThrow()
                         .getDeclaredConstructor(String.class, Map.class, Collection.class)
                         .newInstance(datiLavoro.id, datiLavoro.parametri, datiLavoro.incompatibilità);
        } catch (Exception e) {
            throw new UncheckedIOException(new IOException(String.format("Non è possibile creare un lavoro per '%s'", datiLavoro), e));
        }
    }

    public void registraLavoro(Class<? extends ILavoro> lavoro) {
        classi.add(lavoro);
    }
}
