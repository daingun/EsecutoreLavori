package it.esecutore.generatore;

import it.esecutore.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * Classe che permette la generazione dei lavori partendo da un file di configurazione.
 */
public final class GeneratoreAttività {

    /**
     * Istanza del generatore di lavori.
     */
    private final IGeneratoreLavori generatoreLavori;

    /**
     * Istanza dell'analizzatore della configurazione.
     */
    private final IAnalizzatore analizzatore;

    /**
     * Crea un generatore di attività.
     *
     * @param generatoreLavori generatore di lavori.
     * @param analizzatore     analizzatore della configurazione.
     */
    public GeneratoreAttività(IGeneratoreLavori generatoreLavori, IAnalizzatore analizzatore) {
        this.generatoreLavori = generatoreLavori;
        this.analizzatore = analizzatore;
    }

    /**
     * Genera l'attività di Terminazione che rappresenta la terminazione del processo.
     * Come dipendenze ha tutti il lavori importati dal file di configurazione.
     *
     * @param attività collezione di attività importate dalla configurazione.
     * @return attività che rappresenta la terminazione del processo.
     */
    public static Attività generaTerminazione(Collection<Attività> attività) {
        // Usa un'ArrayList perché è necessario modificare la lista.
        var lavori_disponibili = attività.stream()
                                         .map(a -> a.lavoro.id())
                                         .collect(Collectors.toCollection(ArrayList::new));
        return new Attività(new Terminazione("Terminazione"), lavori_disponibili, new ArrayList<>());
    }

    /**
     * Genera un'attività a partire dalle informazioni testuali.
     *
     * @param datiLavoro dati del lavoro.
     * @return un'attività.
     * @throws UncheckedIOException se la riga non segue le specifiche o se il tipo di lavoro non esiste.
     */
    private Attività generaSingolaAttività(DatiLavoro datiLavoro) throws UncheckedIOException {
        ILavoro lavoro = this.generatoreLavori.generaLavoro(datiLavoro);
        return new Attività(lavoro, datiLavoro.dipendenzeDeboli, datiLavoro.dipendenzeForti);
    }

    /**
     * Genera la collezione di attività a partire dal file di configurazione.
     *
     * @param file file di configurazione.
     * @return collezione di attività da eseguire.
     * @throws IOException quando non è possibile generare la configurazione.
     */
    public Collection<Attività> generaListaAttività(File file) throws IOException {
        try (FileReader fr = new FileReader(file); BufferedReader br = new BufferedReader(fr)) {
            return generaListaAttività(br);
        }
    }

    /**
     * Genera la collezione di attività a partire dal file di configurazione.
     *
     * @param lettore flusso di dati da leggere, il metodo non chiude il flusso.
     * @return collezione di attività da eseguire.
     */
    public Collection<Attività> generaListaAttività(BufferedReader lettore) {
        // Usa un TreeSet in modo da lanciare automaticamente un'eccezione se
        // vengono aggiunti due lavori con lo stesso identificativo.
        var lista = this.analizzatore.generaDatiLavoro(lettore)
                                     .stream()
                                     .map(this::generaSingolaAttività)
                                     .collect(Collectors.toCollection(TreeSet::new));
        lista.add(generaTerminazione(lista));
        return lista;
    }
}
