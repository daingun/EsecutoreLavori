package it.esecutore;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Classe contenente metodi per la verifica della correttezza.
 */
public final class Verifica {
    /**
     * Determina se la lista delle attività contiene un ciclo nelle sue dipendenze.
     *
     * @param listaAttività Lista delle attività da verificare.
     * @return Vero se la lista delle attività contiene un ciclo nelle sue dipendenze.
     */
    public static boolean verificaAciclicità(Collection<Attività> listaAttività) {
        record Nodo(String id, Collection<String> archi) {
        }

        // Converti la lista di attività in dizionario di nodi.
        var nodi = listaAttività.stream()
                                .map(a -> new Nodo(a.lavoro.id(), a.dipendenze().toList()))
                                .collect(Collectors.toMap(Nodo::id, Function.identity()));

        // Conteggio degli archi entranti per ogni nodo.
        var gradoEntrata = nodi.values().stream().collect(Collectors.toMap(Nodo::id, n -> 0));
        for (var nodo : nodi.values()) {
            for (var arco : nodo.archi()) {
                gradoEntrata.merge(arco, 1, Integer::sum);
            }
        }

        // Ricerca i nodi con grado in entrata nullo.
        var liberi = nodi.values().stream()
                         .filter(n -> gradoEntrata.get(n.id()) == 0)
                         .collect(Collectors.toCollection(ArrayDeque::new));

        while (!liberi.isEmpty()) {
            // Finché la collezione di nodi senza archi in entrata non è vuota,
            // prendi uno di questi nodi e decrementa di uno gli archi in entrata dei suoi nodi adiacenti.
            var nodo = liberi.pop();
            for (var arco : nodo.archi()) {
                var ge = gradoEntrata.merge(arco, -1, Integer::sum);
                if (ge == 0) {
                    // Se il nodo non ha più archi in entrata inseriscilo nella collezione.
                    liberi.push(nodi.get(arco));
                }
            }
        }

        // Se esistono nodi con archi in entrata allora significa che è presente un ciclo.
        return gradoEntrata.values().stream().anyMatch(ge -> ge > 0);
    }
}
