package it.esecutore;

import it.esecutore.analizzatore.AnalizzatoreXML;
import it.esecutore.generatore.GeneratoreAttività;
import it.esecutore.generatore.GeneratoreLavori;
import it.esecutore.lavori.Calcolo;
import it.esecutore.lavori.Invio;
import it.esecutore.lavori.PreparazioneDati;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class Main {
    public static void main(String[] args) {
        if (args.length < 1) {
            System.err.println("È necessario specificare il percorso del file di configurazione");
            System.exit(1);
        }

        if (args[0].equals("--aiuto")) {
            stampaAiuto();
            System.exit(0);
        }

        int livelloRegistro = 5;
        if (args.length == 3 && args[1].equals("--livelloRegistro")) {
            livelloRegistro = Integer.parseInt(args[2]);
            if (livelloRegistro < 0 || livelloRegistro > 8) {
                System.err.println("Il livello del registro non esiste. Verrà usato il livello predefinito (INFO).");
            }
        }
        Logger registro = creaRegistro(livelloRegistro);

        String percorsoFile = args[0];
        File configurazione = new File(percorsoFile);
        if (!configurazione.exists()) {
            System.err.printf("Il file '%s' non esiste", percorsoFile);
            System.exit(1);
        }

        GeneratoreLavori generatoreLavori = new GeneratoreLavori();
        try {
            generatoreLavori.registraLavoro(PreparazioneDati.class);
            generatoreLavori.registraLavoro(Calcolo.class);
            generatoreLavori.registraLavoro(Invio.class);
        } catch (Exception e) {
            registro.log(Level.SEVERE, "Impossibile creare trovare la classe per il lavoro", e);
            System.exit(1);
            return;
        }

        IAnalizzatore analizzatore = new AnalizzatoreXML();

        GeneratoreAttività generatoreAttività = new GeneratoreAttività(generatoreLavori, analizzatore);
        Collection<Attività> listaAttività;
        try {
            listaAttività = generatoreAttività.generaListaAttività(configurazione);
            if (listaAttività.size() == 1) {
                registro.log(Level.SEVERE, "La configurazione è vuota o contiene errori.");
                System.exit(1);
                return;
            }
            if (Verifica.verificaAciclicità(listaAttività)) {
                registro.log(Level.SEVERE, "Il grafo delle dipendenze contiene un ciclo");
                System.exit(1);
                return;
            }
        } catch (IOException e) {
            registro.log(Level.SEVERE, "Impossibile leggere il file di configurazione", e);
            System.exit(1);
            return;
        }

        BlockingQueue<ILavoro> codaDiEsecuzione = new LinkedBlockingQueue<>();
        BlockingQueue<Risultato> codaDeiRisultati = new LinkedBlockingQueue<>();

        final int N_THREADS = 4;
        Esecutore esecutore = new Esecutore(codaDiEsecuzione, codaDeiRisultati, N_THREADS);
        Thread esecuzione = new Thread(esecutore::esegui);
        esecuzione.start();

        Direttore direttore = new Direttore(listaAttività, codaDiEsecuzione, codaDeiRisultati);
        direttore.dirigi();

        try {
            esecuzione.join();
        } catch (InterruptedException e) {
            registro.log(Level.SEVERE, "L'esecutore è stato interrotto in modo anomalo", e);
            System.exit(1);
        }

        registro.info("Programma terminato");
        System.exit(0);
    }

    /**
     * Genera il registro delle informazioni del programma usando impostazioni predefinite.
     * <p>
     * Se il livello è minore di zero o maggiore di otto viene usato il valore predefinito di 5 (INFO).
     *
     * @return Registro delle informazioni del programma.
     */
    private static Logger creaRegistro(int livello) {
        Level nuovoLivello = switch (livello) {
            case 0 -> Level.OFF;
            case 1 -> Level.FINEST;
            case 2 -> Level.FINER;
            case 3 -> Level.FINE;
            case 4 -> Level.CONFIG;
            case 7 -> Level.SEVERE;
            case 8 -> Level.ALL;
            case 6 -> Level.WARNING;
            default -> Level.INFO;
        };
        Logger registro = Logger.getLogger("Storico");
        registro.setLevel(nuovoLivello);
        LogManager.getLogManager().addLogger(registro);
        return registro;
    }

    /**
     * Stampa il messaggio di aiuto.
     */
    private static void stampaAiuto() {
        System.out.println("EsecutoreLavori - Utilizzo del programma:");
        System.out.println("    java -jar EsecutoreLavori.jar ./configurazione.xml [opzioni]");
        System.out.println("    java -jar EsecutoreLavori.jar --aiuto");
        System.out.println("Opzioni:");
        System.out.println("    --aiuto : stampa questo aiuto");
        System.out.println("    --livelloRegistro <n> : definisci il livello del registro");
        System.out.println("Livelli del registro:");
        System.out.println("    0: Nessuno");
        System.out.println("    1: Molto dettagliati");
        System.out.println("    2: Più dettagliati");
        System.out.println("    3: Dettagliati");
        System.out.println("    4: Configurazioni");
        System.out.println("    5: Informazioni");
        System.out.println("    6: Avvertenze");
        System.out.println("    7: Gravi");
        System.out.println("    8: Tutti");
    }
}