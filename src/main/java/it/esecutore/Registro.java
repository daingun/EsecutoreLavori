package it.esecutore;

import java.util.logging.LogManager;
import java.util.logging.Logger;

/**
 * Classe per ottenere il registro in uso durante l'esecuzione del programma.
 */
public final class Registro {
    /**
     * Ottieni il registro del programma.
     * <p>
     * Durante l'esecuzione del programma viene restituito il registro definito nella classe Main.
     * <p>
     * Durante i test viene restituito comunque un registro utilizzabile.
     *
     * @return Registro
     */
    public static Logger OttieniRegistro() {
        var registro = LogManager.getLogManager().getLogger("Storico");
        if (registro == null) {
            return Logger.getLogger("test");
        }
        return registro;
    }
}
