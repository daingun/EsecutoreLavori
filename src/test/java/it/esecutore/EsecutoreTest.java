package it.esecutore;

import it.esecutore.analizzatore.AnalizzatoreXML;
import it.esecutore.generatore.GeneratoreAttività;
import it.esecutore.generatore.GeneratoreLavori;
import it.esecutore.lavori.Calcolo;
import it.esecutore.lavori.Invio;
import it.esecutore.lavori.PreparazioneDati;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.StringReader;
import java.util.Collection;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class EsecutoreTest {

    @Test
    public void testEsegui() throws InterruptedException {
        var config = """
                <?xml version="1.0" encoding="UTF-8"?>
                <configurazione>
                    <lavoro>
                        <id>calcolo1</id>
                        <tipo>Calcolo</tipo>
                        <parametri>div:001,errore:propaga</parametri>
                    </lavoro>
                    <lavoro>
                        <id>preparazione1</id>
                        <tipo>PreparazioneDati</tipo>
                        <parametri>div:001</parametri>
                        <dipendenze-forti>calcolo1</dipendenze-forti>
                    </lavoro>
                    <lavoro>
                        <id>invio1</id>
                        <tipo>Invio</tipo>
                        <parametri>div:001</parametri>
                        <dipendenze-forti>preparazione1</dipendenze-forti>
                    </lavoro>
                    <lavoro>
                        <id>calcolo2</id>
                        <tipo>Calcolo</tipo>
                        <parametri>div:002</parametri>
                    </lavoro>
                    <lavoro>
                        <id>preparazione2</id>
                        <tipo>PreparazioneDati</tipo>
                        <parametri>div:002</parametri>
                        <dipendenze-deboli>calcolo1</dipendenze-deboli>
                        <dipendenze-forti>calcolo2</dipendenze-forti>
                    </lavoro>
                    <lavoro>
                        <id>invio2</id>
                        <tipo>Invio</tipo>
                        <parametri>div:002</parametri>
                        <dipendenze-deboli>preparazione1</dipendenze-deboli>
                        <dipendenze-forti>preparazione2</dipendenze-forti>
                    </lavoro>
                </configurazione>""";
        GeneratoreLavori generatoreLavori = new GeneratoreLavori();
        generatoreLavori.registraLavoro(PreparazioneDati.class);
        generatoreLavori.registraLavoro(Calcolo.class);
        generatoreLavori.registraLavoro(Invio.class);
        GeneratoreAttività generatoreAttività = new GeneratoreAttività(generatoreLavori, new AnalizzatoreXML());
        var listaAttività = generatoreAttività.generaListaAttività(new BufferedReader(new StringReader(config)));

        assertEquals(listaAttività.size(), 7);

        BlockingQueue<ILavoro> codaDiEsecuzione = new LinkedBlockingQueue<>();
        BlockingQueue<Risultato> codaDeiRisultati = new LinkedBlockingQueue<>();

        var direttore = new FintoDirettore(listaAttività, codaDiEsecuzione);
        direttore.dirigi();
        assertEquals(codaDiEsecuzione.size(), 7);

        // Il numero di thread deve essere uno in questo modo è garantita l'esecuzione
        // in serie dei lavori e la Terminazione viene sempre eseguita per ultima.
        var esecutore = new Esecutore(codaDiEsecuzione, codaDeiRisultati, 1);
        Thread esecuzione = new Thread(esecutore::esegui);
        esecuzione.start();

        esecuzione.join();

        assertTrue(codaDiEsecuzione.isEmpty());
        assertEquals(codaDeiRisultati.size(), 7);
    }

    private static class FintoDirettore {
        private final Collection<Attività> listaAttività;
        private final BlockingQueue<ILavoro> codaDiEsecuzione;

        private FintoDirettore(Collection<Attività> listaAttività, BlockingQueue<ILavoro> codaDiEsecuzione) {
            this.listaAttività = listaAttività;
            this.codaDiEsecuzione = codaDiEsecuzione;
        }

        private void dirigi() {
            // Ordina i lavori in modo che la Terminazione sia sempre ultima.
            this.listaAttività.stream().map(a -> a.lavoro).sorted((ILavoro l1, ILavoro l2) -> {
                if (l1 instanceof Terminazione) {
                    return 1;
                } else if (l2 instanceof Terminazione) {
                    return -1;
                } else {
                    return 0;
                }
            }).forEach(e -> {
                try {
                    this.codaDiEsecuzione.put(e);
                } catch (InterruptedException ex) {
                    throw new RuntimeException(ex);
                }
            });
        }
    }
}