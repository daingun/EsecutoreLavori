package it.esecutore;

import it.esecutore.generatore.GeneratoreAttività;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.testng.Assert.*;

public class AttivitàTest {

    Attività attività;
    final Collection<Attività> listaAciclicaAttività = new ArrayList<>();
    final Collection<Attività> listaCiclicaAttività = new ArrayList<>();

    @BeforeMethod
    public void creaAttività() {
        var dipendenzeDeboli = new ArrayList<>(List.of("a", "b", "c"));
        var dipendenzeForti = new ArrayList<>(List.of("e", "f"));
        var lavoro = new Terminazione("t");
        this.attività = new Attività(lavoro, dipendenzeDeboli, dipendenzeForti);
    }

    @BeforeClass
    public void creaListaAttivitàAciclica() {
        listaAciclicaAttività.add(new Attività(new LavoroDiTest("a"), new ArrayList<>(List.of()), new ArrayList<>(List.of())));
        listaAciclicaAttività.add(new Attività(new LavoroDiTest("b"), new ArrayList<>(List.of("a")), new ArrayList<>(List.of())));
        listaAciclicaAttività.add(new Attività(new LavoroDiTest("c"), new ArrayList<>(List.of()), new ArrayList<>(List.of("a"))));
        listaAciclicaAttività.add(new Attività(new LavoroDiTest("d"), new ArrayList<>(List.of("b")), new ArrayList<>(List.of())));
        listaAciclicaAttività.add(new Attività(new LavoroDiTest("e"), new ArrayList<>(List.of("b")), new ArrayList<>(List.of("c"))));
        listaAciclicaAttività.add(new Attività(new LavoroDiTest("f"), new ArrayList<>(List.of()), new ArrayList<>(List.of("c"))));
        listaAciclicaAttività.add(GeneratoreAttività.generaTerminazione(listaAciclicaAttività));
    }

    @BeforeClass
    public void creaListaAttivitàCiclica() {
        listaCiclicaAttività.add(new Attività(new LavoroDiTest("a"), new ArrayList<>(List.of()), new ArrayList<>(List.of())));
        // Ciclo
        listaCiclicaAttività.add(new Attività(new LavoroDiTest("b"), new ArrayList<>(List.of("a", "e")), new ArrayList<>(List.of())));
        listaCiclicaAttività.add(new Attività(new LavoroDiTest("c"), new ArrayList<>(List.of()), new ArrayList<>(List.of("a"))));
        listaCiclicaAttività.add(new Attività(new LavoroDiTest("d"), new ArrayList<>(List.of("b")), new ArrayList<>(List.of())));
        listaCiclicaAttività.add(new Attività(new LavoroDiTest("e"), new ArrayList<>(List.of("b")), new ArrayList<>(List.of("c"))));
        listaCiclicaAttività.add(new Attività(new LavoroDiTest("f"), new ArrayList<>(List.of()), new ArrayList<>(List.of("c"))));
        listaCiclicaAttività.add(GeneratoreAttività.generaTerminazione(listaCiclicaAttività));
    }

    @Test
    public void testRimuoviDipendenza() {
        this.attività.rimuoviDipendenza("b", false);
        this.attività.rimuoviDipendenza("f", false);

        assertFalse(this.attività.senzaDipendenze());
        assertFalse(this.attività.inErrore());
    }

    @Test
    public void testSenzaDipendenze() {
        this.attività.rimuoviDipendenza("a", false);
        this.attività.rimuoviDipendenza("b", false);
        this.attività.rimuoviDipendenza("c", false);
        this.attività.rimuoviDipendenza("e", false);
        this.attività.rimuoviDipendenza("f", false);

        assertTrue(this.attività.senzaDipendenze());
    }

    @Test
    public void testDipendenzaForteInErrore() {
        this.attività.rimuoviDipendenza("b", false);
        this.attività.rimuoviDipendenza("f", true);

        assertTrue(this.attività.inErrore());
    }

    @Test
    public void testDipendenzaDeboleInErrore() {
        this.attività.rimuoviDipendenza("b", true);
        this.attività.rimuoviDipendenza("f", false);

        assertFalse(this.attività.inErrore());
    }

    @Test
    public void testVerificaACiclicità() {
        assertEquals(listaAciclicaAttività.size(), 7);
        assertFalse(Verifica.verificaAciclicità(listaAciclicaAttività));
    }

    @Test
    public void testVerificaCiclicità() {
        assertEquals(listaCiclicaAttività.size(), 7);
        assertTrue(Verifica.verificaAciclicità(listaCiclicaAttività));
    }

    /**
     * Classe di tipo ILavoro che esegue la preparazione dei dati.
     *
     * @param id Identificativo.
     */
    private record LavoroDiTest(String id) implements ILavoro {

        @Override
        public Collection<String> incompatibilità() {
            return List.of();
        }

        @Override
        public Risultato lavora() {
            return new Risultato(this.id, Risultato.Tipo.SUCCESSO);
        }
    }
}