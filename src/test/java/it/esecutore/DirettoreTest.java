package it.esecutore;

import it.esecutore.analizzatore.AnalizzatoreXML;
import it.esecutore.generatore.GeneratoreAttività;
import it.esecutore.generatore.GeneratoreLavori;
import it.esecutore.lavori.Calcolo;
import it.esecutore.lavori.Invio;
import it.esecutore.lavori.PreparazioneDati;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.StringReader;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class DirettoreTest {
    @Test
    public void testDirigi() throws InterruptedException {
        var config = """
                <?xml version="1.0" encoding="UTF-8"?>
                <configurazione>
                    <lavoro>
                        <id>calcolo1</id>
                        <tipo>Calcolo</tipo>
                        <parametri>div:001,errore:propaga</parametri>
                    </lavoro>
                    <lavoro>
                        <id>preparazione1</id>
                        <tipo>PreparazioneDati</tipo>
                        <parametri>div:001</parametri>
                        <dipendenze-forti>calcolo1</dipendenze-forti>
                    </lavoro>
                    <lavoro>
                        <id>invio1</id>
                        <tipo>Invio</tipo>
                        <parametri>div:001</parametri>
                        <dipendenze-forti>preparazione1</dipendenze-forti>
                    </lavoro>
                    <lavoro>
                        <id>calcolo2</id>
                        <tipo>Calcolo</tipo>
                        <parametri>div:002</parametri>
                    </lavoro>
                    <lavoro>
                        <id>preparazione2</id>
                        <tipo>PreparazioneDati</tipo>
                        <parametri>div:002</parametri>
                        <dipendenze-deboli>calcolo1</dipendenze-deboli>
                        <dipendenze-forti>calcolo2</dipendenze-forti>
                    </lavoro>
                    <lavoro>
                        <id>invio2</id>
                        <tipo>Invio</tipo>
                        <parametri>div:002</parametri>
                        <dipendenze-deboli>preparazione1</dipendenze-deboli>
                        <dipendenze-forti>preparazione2</dipendenze-forti>
                    </lavoro>
                </configurazione>""";
        GeneratoreLavori generatoreLavori = new GeneratoreLavori();
        generatoreLavori.registraLavoro(PreparazioneDati.class);
        generatoreLavori.registraLavoro(Calcolo.class);
        generatoreLavori.registraLavoro(Invio.class);
        GeneratoreAttività generatoreAttività = new GeneratoreAttività(generatoreLavori, new AnalizzatoreXML());
        var listaAttività = generatoreAttività.generaListaAttività(new BufferedReader(new StringReader(config)));

        BlockingQueue<ILavoro> codaDiEsecuzione = new LinkedBlockingQueue<>();
        BlockingQueue<Risultato> codaDeiRisultati = new LinkedBlockingQueue<>();

        var esecutore = new FintoEsecutore(codaDiEsecuzione, codaDeiRisultati);
        Thread esecuzione = new Thread(esecutore::esegui);
        esecuzione.start();

        assertEquals(listaAttività.size(), 7);

        Direttore direttore = new Direttore(listaAttività, codaDiEsecuzione, codaDeiRisultati);
        direttore.dirigi();

        esecuzione.join();

        assertTrue(codaDiEsecuzione.isEmpty());
        assertTrue(codaDeiRisultati.isEmpty());
    }

    private static class FintoEsecutore {
        private final BlockingQueue<ILavoro> codaDiEsecuzione;
        private final BlockingQueue<Risultato> codaDeiRisultati;

        private FintoEsecutore(BlockingQueue<ILavoro> codaDiEsecuzione, BlockingQueue<Risultato> codaDeiRisultati) {
            this.codaDiEsecuzione = codaDiEsecuzione;
            this.codaDeiRisultati = codaDeiRisultati;
        }

        private void esegui() {
            while (true) {
                try {
                    ILavoro lavoro = this.codaDiEsecuzione.take();
                    boolean termine = lavoro instanceof Terminazione;
                    var risultato = new Risultato(lavoro.id(), termine ? Risultato.Tipo.TERMINAZIONE : Risultato.Tipo.SUCCESSO);
                    this.codaDeiRisultati.put(risultato);
                    if (termine) {
                        return;
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }
}