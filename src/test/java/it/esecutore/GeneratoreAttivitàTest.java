package it.esecutore;

import it.esecutore.analizzatore.AnalizzatoreXML;
import it.esecutore.generatore.GeneratoreAttività;
import it.esecutore.generatore.GeneratoreLavori;
import it.esecutore.lavori.Calcolo;
import it.esecutore.lavori.Invio;
import it.esecutore.lavori.PreparazioneDati;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.StringReader;
import java.io.UncheckedIOException;

import static org.testng.Assert.*;

public class GeneratoreAttivitàTest {

    private GeneratoreAttività generatoreAttività;

    @BeforeClass
    public void creaGeneratoreAttività() {
        GeneratoreLavori generatoreLavori = new GeneratoreLavori();
        generatoreLavori.registraLavoro(PreparazioneDati.class);
        generatoreLavori.registraLavoro(Calcolo.class);
        generatoreLavori.registraLavoro(Invio.class);
        generatoreAttività = new GeneratoreAttività(generatoreLavori, new AnalizzatoreXML());
    }

    @Test
    public void testGeneraAttività() {
        var config = """
                <?xml version="1.0" encoding="UTF-8"?>
                <configurazione>
                    <lavoro>
                        <id>calcolo1</id>
                        <tipo>Calcolo</tipo>
                        <parametri>div:001,errore:propaga</parametri>
                        <dipendenze-deboli></dipendenze-deboli>
                        <dipendenze-forti></dipendenze-forti>
                        <incompatibilità></incompatibilità>
                    </lavoro>
                    <lavoro>
                        <id>preparazione1</id>
                        <tipo>PreparazioneDati</tipo>
                        <parametri>div:001</parametri>
                        <dipendenze-deboli></dipendenze-deboli>
                        <dipendenze-forti>calcolo1</dipendenze-forti>
                        <incompatibilità></incompatibilità>
                    </lavoro>
                    <lavoro>
                        <id>invio1</id>
                        <tipo>Invio</tipo>
                        <parametri>div:001</parametri>
                        <dipendenze-deboli></dipendenze-deboli>
                        <dipendenze-forti>preparazione1</dipendenze-forti>
                        <incompatibilità></incompatibilità>
                    </lavoro>
                    <lavoro>
                        <id>calcolo2</id>
                        <tipo>Calcolo</tipo>
                        <parametri>div:002</parametri>
                        <dipendenze-deboli></dipendenze-deboli>
                        <dipendenze-forti></dipendenze-forti>
                        <incompatibilità></incompatibilità>
                    </lavoro>
                    <lavoro>
                        <id>preparazione2</id>
                        <tipo>PreparazioneDati</tipo>
                        <parametri>div:002</parametri>
                        <dipendenze-deboli>calcolo1</dipendenze-deboli>
                        <dipendenze-forti>calcolo2</dipendenze-forti>
                        <incompatibilità></incompatibilità>
                    </lavoro>
                    <lavoro>
                        <id>invio2</id>
                        <tipo>Invio</tipo>
                        <parametri>div:002</parametri>
                        <dipendenze-deboli>preparazione1</dipendenze-deboli>
                        <dipendenze-forti>preparazione2</dipendenze-forti>
                        <incompatibilità></incompatibilità>
                    </lavoro>
                </configurazione>""";
        var a = generatoreAttività.generaListaAttività(new BufferedReader(new StringReader(config)));
        assertEquals(7, a.size());
    }

    @Test
    public void testGeneraAttivitàDaConfigurazioneVuota() {
        var config = "";
        var a = generatoreAttività.generaListaAttività(new BufferedReader(new StringReader(config)));
        assertEquals(1, a.size());
        assertTrue(a.stream().findFirst().map(x -> x.lavoro instanceof Terminazione).orElse(false));
    }

    @Test
    public void testGeneraAttivitàDaConfigurazioneErrata() {
        var config = """
                <?xml version="1.0" encoding="UTF-8"?>
                <configurazione>
                    <lavoro>
                        <id>calcolo1</id>
                        <tipo>xxxCalcolo</tipo>
                        <parametri>div:001,errore:propaga</parametri>
                        <dipendenze-deboli></dipendenze-deboli>
                        <dipendenze-forti></dipendenze-forti>
                        <incompatibilità></incompatibilità>
                    </lavoro>
                </configurazione>""";
        assertThrows(UncheckedIOException.class, () -> generatoreAttività.generaListaAttività(new BufferedReader(new StringReader(config))));
    }
}